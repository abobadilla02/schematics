/*
@Component({
	selector: 'hello-alonso-bobadilla'
})
export class HelloAlonsoBobadillaComponent {
	console.log('Hola Alonso Bobadilla');	
}
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = '/api/alonso-bobadilla';

@Injectable({
	providedIn: 'root'
})
export class AlonsoBobadillaCrudResourceService {
	constructor(private httpClient: HttpClient) {

	}
  
	findAll(): Observable<AlonsoBobadilla[]> {
		
		return this.httpClient.get<AlonsoBobadilla[]>(API_URL);
		
	}

	create(alonsoBobadilla: AlonsoBobadilla): Observable<number> {
		return this.httpClient.post<number>(API_URL, alonsoBobadilla);
	}
}

export interface AlonsoBobadilla {
	someProperty: string;
}