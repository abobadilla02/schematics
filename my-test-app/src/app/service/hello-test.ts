/*
@Component({
	selector: 'hello-test'
})
export class HelloTestComponent {
	console.log('Hola test');	
}
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = '/api/test';

@Injectable({
	providedIn: 'root'
})
export class TestCrudResourceService {
	constructor(private httpClient: HttpClient) {

	}
  
	findAll(): Observable<Test[]> {
		
		return "hola";
		
	}

	create(test: Test): Observable<number> {
		return this.httpClient.post<number>(API_URL, test);
	}
}

export interface Test {
	someProperty: string;
}