/*
@Component({
	selector: 'hello-reproduccion-tests'
})
export class HelloReproduccionTestsComponent {
	console.log('Hola reproduccion tests');	
}
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = '/api/reproduccion-tests';

@Injectable({
	providedIn: 'root'
})
export class ReproduccionTestsCrudResourceService {
	constructor(private httpClient: HttpClient) {

	}
  
	findAll(): Observable<ReproduccionTests[]> {
		
		return "hola";
		
	}

	create(reproduccionTests: ReproduccionTests): Observable<number> {
		return this.httpClient.post<number>(API_URL, reproduccionTests);
	}
}

export interface ReproduccionTests {
	someProperty: string;
}