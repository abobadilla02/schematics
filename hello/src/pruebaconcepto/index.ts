import { Rule, SchematicContext, Tree, apply, mergeWith, template, url, SchematicsException, move } from '@angular-devkit/schematics';
import { strings } from '@angular-devkit/core';
import { parseName } from '@schematics/angular/utility/parse-name';
import { buildDefaultPath } from '@schematics/angular/utility/project';

import { Schema } from './schema';

export function pruebaconcepto(_options: Schema): Rule {

  return (tree: Tree, _context: SchematicContext) => {
    const workspaceConfigBuffer = tree.read("angular.json");
    if ( !workspaceConfigBuffer ) {
      throw new SchematicsException("No está en un espacio de trabajo de Angular CLI")
    }

    const workspaceConfig = JSON.parse(workspaceConfigBuffer.toString());
    const projectName = _options.project || workspaceConfig.defaultProject;
    const project = workspaceConfig.projects[projectName];

    const defaultProjectPath = buildDefaultPath(project);

    const parsedPath = parseName(defaultProjectPath, _options.name)

    const { name, path } = parsedPath;

  	const sourceTemplate = url('./files');

  	const sourceParametrizedTemplate = apply(sourceTemplate, [
  		template({
  			..._options,
  			...strings,
        name
  		}),
      move(path)
		]);

		return mergeWith(sourceParametrizedTemplate)(tree, _context);
  };
}
