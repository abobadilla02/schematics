/*
@Component({
	selector: 'hello-<%= dasherize(name) %>'
})
export class Hello<%= classify(name) %>Component {
	console.log('Hola <%= name %>');	
}
*/

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const API_URL = '/api/<%= dasherize(name) %>';

@Injectable({
	providedIn: 'root'
})
export class <%= classify(name) %>CrudResourceService {
	constructor(private httpClient: HttpClient) {

	}
  
	findAll(): Observable<<%= classify(name) %>[]> {
		<% if ( transform ) { %>
		return this.httpClient.get<<%= classify(name) %>[]>(API_URL);
		<% } else { %>
		return "hola";
		<% } %>
	}

	create(<%= camelize(name) %>: <%= classify(name) %>): Observable<number> {
		return this.httpClient.post<number>(API_URL, <%= camelize(name) %>);
	}
}

export interface <%= classify(name) %> {
	someProperty: string;
}